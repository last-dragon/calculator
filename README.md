# Виджет калькулятора. Проект на React
**Задание первое: использование компонентов на основе классов**
___
Верстка по [макету](https://www.figma.com/file/QDMTwIRLQuK8bkyZmmztuT/Calculator-(Community)?node-id=0%3A1&mode=dev)
+ Демонстрация: https://calculator-chi-eosin.vercel.app/
___
## Используемые технологии:
+ React
+ CSS modules
    + Normalize.css
    + Адаптивная верстка
___
## Для запуска проекта:
1. Клонируйте проект и установите зависимости

npm install

2. Запуск проекта

npm start