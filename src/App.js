import React from 'react';
import Display from "./components/Display"
import Button from "./components/Button";
import Keyboard from "./components/Keyboard";
import { handleDigitClick, handleOperatorClick, handleEqualClick, handleClearClick } from "./utils/func";
import './Calculator.css';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      displayValue: '0',
      operator: null,
      firstOperand: null,
      waitingForSecondOperand: false
    };
  }

  handleDigitClick = (digit) => {
    handleDigitClick.call(this, digit);
  };

  handleOperatorClick = (operator) => {
    handleOperatorClick.call(this, operator);
  };

  handleEqualClick = () => {
    handleEqualClick.call(this);
  };

  handleClearClick = () => {
    handleClearClick.call(this);
  };

  render() {
    const { displayValue } = this.state;

    return (
      <>
      <h1>Виджет калькулятора</h1>
      <div className="calculator" tabIndex={0}>
        <Display displayValue={displayValue} />
        <div className="buttons">
          <Button onClick={this.handleClearClick } className="clear-button" value="AC" />
          <Button onClick={this.handleOperatorClick} className="multiply-button" value="*" />
          <Button onClick={this.handleOperatorClick} className="minus-button" value="-" />
          <Button onClick={this.handleOperatorClick} className="plus-button" value="+" />
          <Button onClick={this.handleOperatorClick} className="divide-button" value="/" />
          <Button onClick={this.handleDigitClick} className="one-button" value="1" />
          <Button onClick={this.handleDigitClick} className="two-button" value="2" />
          <Button onClick={this.handleDigitClick} className="three-button" value="3" />
          <Button onClick={this.handleDigitClick} className="four-button" value="4" />
          <Button onClick={this.handleDigitClick} className="five-button" value="5" />
          <Button onClick={this.handleDigitClick} className="six-button" value="6" />
          <Button onClick={this.handleDigitClick} className="seven-button" value="7" />
          <Button onClick={this.handleDigitClick} className="eight-button" value="8" />
          <Button onClick={this.handleDigitClick} className="nine-button" value="9" />
          <Button onClick={this.handleDigitClick} className="zero-button" value="0" />
        </div>
        <div className="last-buttons-container">
          <Button onClick={this.handleEqualClick} value="=" className="equals-button" />
          <Button onClick={this.handleDigitClick} value="." className="point-button"/>
        </div>
        <Keyboard
          handleDigitClick={this.handleDigitClick}
          handleOperatorClick={this.handleOperatorClick}
          handleEqualClick={this.handleEqualClick}
          handleClearClick={this.handleClearClick}
        />
      </div>
      </>
    );
  }
}

export default App;
