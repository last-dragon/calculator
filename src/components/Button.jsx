import React from "react";

class Button extends React.Component {
  handleClick = () => {
    const { onClick, value } = this.props;
    onClick(value);
  };

  render() {
    const { className } = this.props;
    return <button className={className} onClick={this.handleClick}></button>;
  }
}

export default Button;
