import React from "react";

class Display extends React.Component {
  render() {
    const { displayValue } = this.props;
    return <div className="display">{displayValue}</div>;
  }
}

export default Display;
