import React from 'react';

class Keyboard extends React.Component {
  componentDidMount() {
    document.addEventListener('keydown', this.handleKeyDown);
  }

  componentWillUnmount() {
    document.removeEventListener('keydown', this.handleKeyDown);
  }

  handleKeyDown = (event) => {
    const key = event.key;
    const {
      handleDigitClick,
      handleOperatorClick,
      handleEqualClick,
      handleClearClick,
    } = this.props;

    if (/\d/.test(key)) {
      // Нажата цифровая клавиша
      handleDigitClick(key);
    } else if (['+', '-', '*', '/'].includes(key)) {
      // Нажата клавиша операции
      handleOperatorClick(key);
    } else if (key === '=' || key === 'Enter') {
      // Нажата клавиша равно или Enter
      handleEqualClick();
    } else if (key === 'Escape') {
      // Нажата клавиша "Очистить" (Escape)
      handleClearClick();
    }

    event.preventDefault(); // Отменить стандартное действие браузера
  };

  render() {
    return null;
  }
}

export default Keyboard;
