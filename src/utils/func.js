// Функция для обработки нажатия на цифровую кнопку
function handleDigitClick(digit) {
    const { waitingForSecondOperand } = this.state;

    if (waitingForSecondOperand) {
      this.setState({
        displayValue: digit === '.' ? '0.' : digit.toString(),
        waitingForSecondOperand: false
      });
    } else {
      this.setState(prevState => ({
        displayValue:
          prevState.displayValue === '0' ? digit.toString() : prevState.displayValue + digit.toString()
      }));
    }
}

// Функция для обработки нажатия на кнопку оператора
function handleOperatorClick(operator) {
    const { displayValue } = this.state;

    this.setState(prevState => ({
      firstOperand: parseFloat(displayValue),
      operator,
      waitingForSecondOperand: true,
      displayValue: prevState.displayValue === '0' ? displayValue : prevState.displayValue
    }));
}

// Функция для обработки нажатия на кнопку равно
function handleEqualClick() {
    const { operator, displayValue, firstOperand } = this.state;
    const secondOperand = parseFloat(displayValue);
    let result;

    if (operator) {
      switch (operator) {
        case '+':
          result = firstOperand + secondOperand;
          break;
        case '-':
          result = firstOperand - secondOperand;
          break;
        case '*':
          result = firstOperand * secondOperand;
          break;
        case '/':
          result = firstOperand / secondOperand;
          break;
        default:
          result = secondOperand;
          break;
      }

      this.setState({
        displayValue: result.toString(),
        firstOperand: result !== 0 ? result : null,
        operator: null,
        waitingForSecondOperand: false
      });
    }
}

// Функция для обработки нажатия на кнопку "Очистить"
function handleClearClick() {
    this.setState({
        displayValue: '0',
        operator: null,
        firstOperand: null,
        waitingForSecondOperand: false
    });
}
  
export { handleDigitClick, handleOperatorClick, handleEqualClick, handleClearClick };  